# Opis
## Przeznaczenie
Aplikacja jest szablonem bloga, stworzonym z wykorzystaniem Reacta oraz Material-UI. Posiada dodatkowo podstawową
integrację z zewnętrznymi API - generującym zdanie dnia oraz zwracającym obraz w danym wymiarze.
Zrzuty ekranu wykonane z aplikacji w celu pokazania funkcjonowania zewnętrznego API są dostępne w folderze
`/img`.

## Konfiguracja i uruchomienie
Konfiguracja odbywa się za pośrednictwem `npm install`, który służy do zainstalowania wszystkich
zależności zawartych w `packages.json`. Po procesie instalacji można uruchomić aplikację w trybie
`development` za pośrednictwem `npm start`, zgodnie z opisem poniżej.

**Uwaga! API służące do pobierania zdania dnia posiada dzienny limit wywołań ustawiony na 10 requestów.
W przypadku przekroczenia tej liczby aplikacja nie zadziała zgodnie z oczekiwaniami.**

# Instrukcje wygenerowane przez generator Reacta
## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br />
You will also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.<br />
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run build`

Builds the app for production to the `build` folder.<br />
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br />
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

## Learn More

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

To learn React, check out the [React documentation](https://reactjs.org/).

### Code Splitting

This section has moved here: https://facebook.github.io/create-react-app/docs/code-splitting

### Analyzing the Bundle Size

This section has moved here: https://facebook.github.io/create-react-app/docs/analyzing-the-bundle-size

### Making a Progressive Web App

This section has moved here: https://facebook.github.io/create-react-app/docs/making-a-progressive-web-app

### Advanced Configuration

This section has moved here: https://facebook.github.io/create-react-app/docs/advanced-configuration

### Deployment

This section has moved here: https://facebook.github.io/create-react-app/docs/deployment

### `npm run build` fails to minify

This section has moved here: https://facebook.github.io/create-react-app/docs/troubleshooting#npm-run-build-fails-to-minify
