import React from 'react';
import axios from "axios";


class Home extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            qod: ''
        }
    }

    async getSentences() {
        await axios.get(
            'https://quotes.rest/qod?language=en',
            {
                headers:
                    {
                        'Content-Type': 'application/json'
                    }
            },
        ).then(response => {
            console.log(response.data);
            this.setState({
                qod: response.data.contents.quotes[0].quote
            });
        }).catch(error => {
            console.log(error)
        });
    }

    componentDidMount(){
        this.getSentences();
    }

    render() {
        return (
            <div>
                <h1>Quote of the day: </h1>
                <blockquote>{this.state.qod}</blockquote>
            </div>
        )
    }
}

export default Home