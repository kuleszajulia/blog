import React from 'react'
import {Route} from "react-router-dom";
import MenuAppBar from "./appbar/appbar";
import Users from "./users";
import Home from "./home/home";
import Blog from "./blog/blog";
import Portfolio from "./portfolio/portfolio";
import AboutMe from "./aboutme/about-me";
import {makeStyles} from "@material-ui/core/styles";

const useStyles = makeStyles({
    content: {
        margin: 30,
    },
});

export default function App() {
    const classes = useStyles();

    return (
        <div>
            <MenuAppBar/>
            <div className={classes.content}>
                <Route exact path="/" component={Home} />
                <Route path="/users" component={Users} />
                <Route path="/blog" component={Blog} />
                <Route path="/portfolio" component={Portfolio} />
                <Route path="/about-me" component={AboutMe} />
            </div>
        </div>
    )
}