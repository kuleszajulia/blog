import React from 'react';
import axios from 'axios';
import Button from '@material-ui/core/Button';

class Users extends React.Component {
    state = {
        users: ['Default Value']
    };

    constructor(props) {
        super(props);
        this.getUsers = this.getUsers.bind(this);
    }


    render() {
        return (
            <div>
                <h1>Users</h1>
                <Button variant="contained" color="primary" onClick={this.getUsers}>Click me!</Button>
                <p >Users: {this.state.users}</p>
            </div>
        )
    }

    async getUsers() {
        await axios.get(
            "http://localhost:8080/blog_posts",
            {
                headers:
                    {
                        'Content-Type': 'application/json',
                        'x-access-token': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjVlMGU2Mjc1NTc3YThkMGI5Y2I2OTNhNiIsImlhdCI6MTU3ODU3ODM5NCwiZXhwIjoxNTc4NjY0Nzk0fQ.1lT81sYWo5RFPFOIuwLKCed-5Otlq9N4q7wudy97Goo'
                    }
            }
            ).then(response => {
                console.log(response.data);
                this.setState({
                    users: response.data.toString()
                });
            }).catch(error => {
                console.log(error)
            });
    }
}

export default Users