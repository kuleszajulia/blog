import React from "react";
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import FavoriteIcon from '@material-ui/icons/Favorite';
import ShareIcon from '@material-ui/icons/Share';
import Grid from "@material-ui/core/Grid";

const useStyles = makeStyles({
    card: {
        maxWidth: 345,
    },
    showButton: {
        marginLeft: 'auto !important',
    }
});

export default function BlogPostCard(props) {
    const classes = useStyles();
    const blogPosts = props.blogPosts;

    const blogPostCards = (blogPosts) => {
        let cards = [];
        blogPosts.forEach(function (blogPost, i) {
            cards.push(
                <Grid key={i} item xs={6} sm={3}>
                    <Card className={classes.card}>
                        <CardActionArea>
                            <CardMedia
                                component="img"
                                alt={blogPost.imgAltText}
                                height="140"
                                image={blogPost.img}
                                title={blogPost.imgAltText}
                            />
                            <CardContent>
                                <Typography gutterBottom variant="h5" component="h2">
                                    {blogPost.title}
                                </Typography>
                                <Typography variant="body2" color="textSecondary" component="p">
                                    {blogPost.text}
                                </Typography>
                            </CardContent>
                        </CardActionArea>
                        <CardActions>
                            <div>
                                <IconButton aria-label="add to favorites">
                                    <FavoriteIcon />
                                </IconButton>
                                <IconButton aria-label="share">
                                    <ShareIcon />
                                </IconButton>
                            </div>
                            <Button className={classes.showButton} size="small" color="primary">
                                Wyświetl
                            </Button>
                        </CardActions>
                    </Card>
                </Grid>
            );
        });
        return cards;
    };

    return (
        <div>
            <Grid container spacing={2}>
                {blogPostCards(blogPosts)}
            </Grid>
        </div>
    );
}