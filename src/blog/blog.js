import React from "react";
import BlogPostCard from "./blog-post-card/blog-post-card";

export default function Blog() {
    const blogPosts = [
        {
            title: 'Blog1',
            text: 'Tekst postu nr 1',
            img: 'https://loremflickr.com/320/240',
            imgAltText: 'Obrazek 1'
        },
        {
            title: 'Blog2',
            text: 'Tekst postu nr 2',
            img: 'https://loremflickr.com/320/240/paris',
            imgAltText: 'Obrazek 2'
        }];

    return (
        <div>
            <BlogPostCard blogPosts={blogPosts}/>
        </div>
    );

}