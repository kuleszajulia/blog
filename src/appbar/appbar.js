import React, {useLayoutEffect, useState} from 'react';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import AccountCircle from '@material-ui/icons/AccountCircle';
import ExpandableMenu from "./expandable-menu";
import './appbar.css';
import ButtonsMenu from "./buttons-menu";

function useWindowSize() {
    const [size, setSize] = useState([0, 0]);
    useLayoutEffect(() => {
        function updateSize() {
            setSize([window.innerWidth, window.innerHeight]);
        }
        window.addEventListener('resize', updateSize);
        updateSize();
        return () => window.removeEventListener('resize', updateSize);
    }, []);
    return size;
}

export default function MenuAppBar() {
    const [width, height] = useWindowSize();
    const urls = {
        'home': '/',
        'blog': '/blog',
        'portfolio': '/portfolio',
        'o mnie': '/about-me',
        'kontakt': '/contact'
    };

    return (
        <div className="root">
            <AppBar position="static">
                <Toolbar>
                    {width < 800 ? (
                        <ExpandableMenu urls={urls}/>
                    ) : (
                        <ButtonsMenu urls={urls}/>
                    )}
                    <div>
                        <IconButton
                            aria-label="account of current user"
                            aria-controls="menu-appbar"
                            aria-haspopup="true"
                            color="inherit"
                        >
                            <AccountCircle />
                        </IconButton>
                    </div>
                </Toolbar>
            </AppBar>
        </div>
    );
}