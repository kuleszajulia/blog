import React from 'react';
import {useHistory } from 'react-router-dom';
import Button from "@material-ui/core/Button";
import './appbar.css';

function ButtonsMenu(props) {
    const history = useHistory();
    const urls = props.urls;

    const handleClick = (url) => {
        history.push(url);
    };

    const createMenuItems = (urls) => {
        let items = [];
        for (const [itemName, url] of Object.entries(urls)) {
            items.push(
                <Button color="inherit" key={itemName} onClick={() => handleClick(url)}>
                    {itemName}
                </Button>
            );
        }
        return items;
    };

    return (
      <div className="menuButton">
          {createMenuItems(urls)}
      </div>
    );

}

export default ButtonsMenu