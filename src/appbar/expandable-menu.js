import React from 'react';
import { useHistory } from "react-router-dom";
import MenuItem from "@material-ui/core/MenuItem";
import IconButton from "@material-ui/core/IconButton";
import MenuIcon from "@material-ui/icons/Menu";
import Menu from "@material-ui/core/Menu";
import './appbar.css';

function ExpandableMenu(props) {
    const [anchorEl, setAnchorEl] = React.useState(null);
    const history = useHistory();
    const urls = props.urls;

    const handleClick = (event: React.MouseEvent<HTMLButtonElement>) => {
        setAnchorEl(event.currentTarget);
    };

    const handleClose = (url) => {
        history.push(url);
        setAnchorEl(null);
    };

    const createMenuItems = (urls) => {
        let items = [];
        for (const [itemName, url] of Object.entries(urls)) {
            items.push(
                <MenuItem key={itemName} onClick={() => handleClose(url)}>
                    {itemName}
                </MenuItem>
            );
        }
        return items;
    };

    return (
        <div className="menuButton">
            <IconButton
                edge="start"
                color="inherit"
                aria-label="menu"
                onClick={handleClick}>
                <MenuIcon/>
            </IconButton>
            <Menu
                id="simple-menu"
                anchorEl={anchorEl}
                keepMounted
                open={Boolean(anchorEl)}
                onClose={handleClose}
            >
                {createMenuItems(urls)}
            </Menu>
        </div>
    );
}

export default ExpandableMenu